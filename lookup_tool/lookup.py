import os
from pymongo import MongoClient

DB_HOST = os.environ.get('DB_HOST', 'mongodb')
DB_PORT = int(os.environ.get('DB_PORT', 27017))
DB_NAME = os.environ.get('DB_NAME', 'lookup')

collection_name = 'field_value_lookup'


# initialize mongodb connection
def init_db():
    client = MongoClient(DB_HOST, DB_PORT)
    db = client[DB_NAME]
    return db


db = init_db()


def lookup_or_set(field, value) -> object:
    # get field dict, check if value is in it
    field_dict = db[collection_name].find_one()

    # if field does not exist, create it with value
    if not field_dict:
        db[collection_name].insert_one({field: {value: 1}})
        return 1

    # if value is in field dict, return the key
    if field in field_dict and value in field_dict[field]:
        return field_dict[field][value]

    else:
        # if value is not in field dict, add it, use autoincrement for key
        # get max key value

        if field not in field_dict:
            field_dict[field] = {}
            max_key = 1
        else:
            max_key = max(list(field_dict[field].values()))
            # increment max key value
            max_key += 1

        # update field dict
        field_dict[field][value] = max_key

        # update field dict in db
        db[collection_name].update_one({}, {"$set": field_dict})

        # return new key value
        return max_key
