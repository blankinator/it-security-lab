from fastapi import FastAPI
import uvicorn

from lookup import lookup_or_set

app = FastAPI()


@app.get("/{field_name}/{value}")
def lookup(field_name: str, value: str):
    return lookup_or_set(field_name, value)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
